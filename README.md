Awam - Calculator

Consigne : Voici les résultats à obtenir :
100 dollars + 100 dollars = 200 dollars
10 euros + 5 euros = 15 euros
100 dollars + 5 euros = 95euros

Objectif visuel :
2 input de valeur
2 input de devise
Un bouton Calculer

L'objectif fonctionnel :
Le résultat doit être celui énoncé au dessus

Objectif de temps : 2-3H

A noter que pour le test, le taux de change pour chaque devise sera fixe. (Ne pas prévoir de taux de change variable)

Il est tout aussi important d'expliquer la démarche technique aborder
et la démarche technique si tu avais 10H de temps sur le sujet.

Il me faut soit un zip, soit un git au choix.
Le test doit-être réaliser en PHP (natif ou avec framework au choix)

N.B :
- Le client est ambitieux, il voudra sûrement utiliser ce petit outil pour différentes devises à terme.
J'ai besoin de savoir comment nous pouvons faire évoluer l'outil assez simplement. (explication à fournir dans la démarche)

Autres informations à prendre en compte :
- Pour finir le client souhaite recevoir l'historique des calculs du jour par Email 
(pour simplifier nous utiliserons la fonction mail de PHP sans configuration spécifique SMTP ou autres)

Démarche : 

En 2h => 1 partie logique qui comprenait une condition de avec trois résultats possible. Chaque condition doit être traité dans le meme if else ou avec un switch case. La première condition impliquait 2 valeur avec comme devise le dollars, la seconde 2 valeurs avec comme devise l'euro, la troisieme 2 valueur avec 2 devises. Cependant le résultats devait retourner de l'euro. 
Une gestion d'erreur a été rajouté en cas ou l'utilisateur oublierai un champs ou une devise.

En 10h => Avec beaucoup plus de temps on pourrait étendre à plusieurs devises, regroupées en tableau voire en objet. Genre Id, Name, Valeur. Il suffirait de loop dans l'objet pour récup les valeurs et les amener dans la fonction de calcul. On pourrait meme transformer tout les valeurs du calcul en object avec en plus la date. La date nous permettrai de ne selectionner que les calculs du jour voulu. On pourrait ajouter des fonctions de calcul de TVA, ou d'autres opérations plus complexes On pourrait intégré plus de style. On pourrait importer plus de valeurs pas uniquement des devise. Bref ca pourrait devenir une vrai calculatrice.   

Merci pour ce petit exercice c'etait marrant. j'espère qu'il répond à tes attentes. 
Bonne soirée/journée









