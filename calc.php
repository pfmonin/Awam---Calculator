<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.3/css/bulma.min.css">
</head>
<body>

    <style>
        .select:not(.is-multiple):not(.is-loading)::after { display:none; }
    </style>

    <div id="devise" class="container is-fluid mt-5">
        <h1 class="title">Supercomputer</h1>

    <form method="post"> 
         
        <div class="field">
            <label class="label mb-4 pl-0">Enter first number</label>  
           <div class="columns ml-0">
                <input class="input column" type="number" name="number1" placeholder="Ex : 100" />        
                <div class="select column p-0 ml-2">
                    <select class="px-3 has-text-center" name="devise" id="">
                        <option value="">-- Choisir une devise --</option>
                        <option value="euro">euro</option>
                        <option value="dollars">dollars</option>
                    </select>
                </div>  
           </div>
        </div> 
        

        <div class="field">
            <label class="label mb-4 pl-0">Enter second number</label>  
           <div class="columns ml-0">
                <input class="input column" type="number" name="number2" placeholder="Ex : 100" />        
                <div class="select column p-0 ml-2">
                    <select class="px-3 has-text-center" name="deviseDeux" id="">
                        <option value="">-- Choisir une devise --</option>
                        <option value="euro">euro</option>
                        <option value="dollars">dollars</option>
                    </select>
                </div>  
           </div>
        </div> 

        <input class="button is-primary mt-5" type="submit" name="submit" value="Calculate">  
        </form>  

        
        <?php  

        $sumEuro = null;
        $sumDollars = null;
        $subDollarsEuro = null;


            if(isset($_POST['submit'])) {                 

                $number1 = $_POST['number1']; 
                $number2 = $_POST['number2'];
                $devise = $_POST['devise'];
                $deviseDeux = $_POST['deviseDeux'];

            if($devise === "euro" && $deviseDeux === "euro") { 
                    $sumSameDevise =  $number1+$number2;                    
                    $sumEuro =  $sumSameDevise . "euro";
                    echo "<p class='mt-3'>" . $sumSameDevise . "euro </p>"; 
                }
                elseif($devise === "dollars" && $deviseDeux === "dollars") {
                    $sumSameDevise =  $number1+$number2;
                    $sumDollars = $sumSameDevise . "dollars";  
                    echo "<p class='mt-3'>" . $sumSameDevise . "dollars </p>";  
                }
                elseif ($devise === "dollars" && $deviseDeux === "euro") {
                    $subDiffDevise =  $number1-$number2; 
                    $subDollarsEuro =  $subDiffDevise . "euro";
                    echo "<p class='mt-3'>" . $subDiffDevise . "euro </p>";
                }
                else {
                    if(!isset($_POST['number1']) || !isset($_POST['number2']) ) {                        
                        echo('At least one value is missing !');
                    }
                    elseif(!isset($_POST['devise']) || !isset($_POST['deviseDeux']) ) {
                        echo('<p class="mt-3">You need to choose both currency ! </p>');
                    }
                    else {
                        echo('<p class="mt3"> Time for a break!</p>');
                    }
                }
            }  


            // Envoi de mail 
            $to = "contact@contact.fr";
            $subject = "Daily currency calculation";

            $message = "
                <html>
                    <head>
                        <title>Daily currency calculation</title>
                    </head>
                <body>
                <h1>Hi sir as requested, here's our daily calculation</h1>
                <p> Addition of two sum from the same currency - Euro " .  $sumEuro ." </p>
                <p> Addition of two sum from the same currency - Dollars " .  $sumDollars ." </p>
                <p> Substraction of two sum from the same currency - Dollars " .  $subDollarsEuro ." </p>
                </body>
                </html>
            ";

            $headers = "Content-type:text/html;charset=UTF-8" . "\r\n";
            mail($to,$subject,$message,$headers);
            
        ?>  

    </div>
    
</body>
</html>